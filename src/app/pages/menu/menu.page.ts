import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  pages = [
    { title: 'Home', url: '/menu/main', icon: 'home'},
    // { title: 'My Appointments', icon: 'phone-portrait', url: '/menu/myappointments' },
    { title: 'Profile', icon: 'person', url: '/menu/profile'},
    // { title: 'Yes Madam Wallet', icon: 'cash', url: '/menu/wallet'},
    { title: 'Our Team', icon: 'people', url: '/menu/ourteam'},
    { title: 'Gallery', icon: 'images', url: '/menu/gallery'},
    { title: 'Events', icon: 'calendar', url: '/menu/events'},
    { title: 'Donators Profile', icon: 'people', url: '/menu/donators'},
    { title: 'Donate Us', icon: 'cash', url: '/menu/donateus'},
    { title: 'About Us', icon: 'help', url: '/menu/faqs'},
    { title: 'Contact Us', icon: 'call', url: '/menu/callus'},
    { title: 'LogOut', icon: 'log-out', url: '/menu/logout'}
  ];
  constructor() { }

  ngOnInit() {
  }

}
