import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/menu/main',
    pathMatch: 'full'
  },
  {
    path: '',
    component: MenuPage,
    children: [
      { path: 'main', loadChildren: '../main/main.module#MainPageModule' },
      { path: 'myappointments', loadChildren: '../my-appointments/my-appointments.module#MyAppointmentsPageModule' },
      { path: 'profile', loadChildren: '../profile/profile.module#ProfilePageModule' },
      { path: 'ourteam', loadChildren: '../our-team/our-team.module#OurTeamPageModule' },
      { path: 'gallery', loadChildren: '../gallery/gallery.module#GalleryPageModule' },
      { path: 'events', loadChildren: '../events/events.module#EventsPageModule' },
      { path: 'donators', loadChildren: '../donators/donators.module#DonatorsPageModule' },
      { path: 'donateus', loadChildren: '../donate-us/donate-us.module#DonateUsPageModule' },
      { path: 'faqs', loadChildren: '../faqs/faqs.module#FaqsPageModule' },
      { path: 'callus', loadChildren: '../call-us/call-us.module#CallUsPageModule' },
      // { path: 'wallet', loadChildren: '../wallet/wallet.module#WalletPageModule' },
      { path: 'address', loadChildren: '../address/address.module#AddressPageModule' },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
