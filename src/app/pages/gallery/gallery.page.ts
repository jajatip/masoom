import { Component, OnInit } from '@angular/core';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.page.html',
  styleUrls: ['./gallery.page.scss'],
  providers:[PhotoViewer]
})
export class GalleryPage implements OnInit {

  constructor(private photoViewer: PhotoViewer) { }

  ngOnInit() {
  }

  showImg(image: string) {
    this.photoViewer.show('https://raw.githubusercontent.com/mjhea0/node-postgres-todo/master/_blog/node-todo-postges.jpg');
  }

}
