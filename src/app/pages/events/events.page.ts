import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {
  sliderOpts = {
    zoom: false,
    slidesPerView: 1.5,
    spaceBetween: 5,
    //centeredSlides: true,
    pager: true
  };
  images = [
    'https://homepages.cae.wisc.edu/~ece533/images/pool.png',
    'https://homepages.cae.wisc.edu/~ece533/images/tulips.png',
    'https://homepages.cae.wisc.edu/~ece533/images/watch.png',
    'https://homepages.cae.wisc.edu/~ece533/images/monarch.png',
    'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'
  ]
  constructor() { }

  ngOnInit() {
  }

}
